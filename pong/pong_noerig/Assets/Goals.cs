using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goals : MonoBehaviour {
    void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "Ball") {
            string wallName = transform.name;
            GameManager.Score(wallName);
            other.gameObject.SendMessage("RestartGame", 1.0f, SendMessageOptions.RequireReceiver);
            SoundManagerScript.PlaySound("goal");
        }
    }
}
