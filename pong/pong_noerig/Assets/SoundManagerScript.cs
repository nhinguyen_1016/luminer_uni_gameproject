using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour {

    public static AudioClip booster, booster2, goal, hit1, hit2;
    public static AudioSource audioSrc;

    // Start is called before the first frame update
    void Start() {
        booster = Resources.Load<AudioClip>("booster1");
        booster2 = Resources.Load<AudioClip>("booster2");
        goal = Resources.Load<AudioClip>("goal");
        hit1 = Resources.Load<AudioClip>("hit1");
        hit2 = Resources.Load<AudioClip>("hit2");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound( string Clip) {
        switch(Clip) {
            case "booster1":
                audioSrc.PlayOneShot(booster);
                break;
            case "booster2":
                audioSrc.PlayOneShot(booster2);
                break;
            case "goal":
                audioSrc.PlayOneShot(goal);
                break;
            case "hit1":
                audioSrc.PlayOneShot(hit1);
                break;
            case "hit2":
                audioSrc.PlayOneShot(hit2);
                break;
        }
    }
}
