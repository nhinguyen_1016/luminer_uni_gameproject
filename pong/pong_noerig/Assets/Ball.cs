using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public float speed;
    Vector2 direction;
    private Rigidbody2D rb;


    void Start() {
        rb = GetComponent<Rigidbody2D>();
        direction = Vector2.one.normalized;
    }

    private void FixedUpdate() {
        rb.velocity = direction * speed;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.CompareTag("Wall")) {
            direction.y = -direction.y;
        } else if(other.gameObject.CompareTag("Paddle")) {
            float rand = Random.Range(0f,1f);
            if(rand > 0.5) {
                SoundManagerScript.PlaySound("hit1");
            } else {
                SoundManagerScript.PlaySound("hit2");
            }
            direction.x = -direction.x;
        }
    }

    void ResetBall(){
        rb.velocity = Vector2.zero;
        transform.position = Vector2.zero;
    }


    void RestartGame(){
        ResetBall();
    }

}
