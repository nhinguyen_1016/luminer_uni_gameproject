using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {
    public bool isPlayer1;
    public float speed;
    public Rigidbody2D rb;
    public float boundY = 3;
    public Animator animatorLeft;
    public Animator animatorRight;
    private float movement;
    Vector2 vectorMov;

    void Start () {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        if (isPlayer1) {
            movement = Input.GetAxisRaw("Vertical");
            vectorMov.x = Input.GetAxisRaw("Vertical");
            vectorMov.y = Input.GetAxisRaw("Vertical");
            animatorLeft.SetFloat("Vertical", vectorMov.y);
            playSound(vectorMov.y, "booster1");
            animatorLeft.SetFloat("Speed", vectorMov.sqrMagnitude);
        } else {
            movement = Input.GetAxisRaw("Vertical2");
            vectorMov.x = Input.GetAxisRaw("Vertical2");
            vectorMov.y = Input.GetAxisRaw("Vertical2");
            animatorRight.SetFloat("Vertical", vectorMov.y);
            playSound(vectorMov.y, "booster2");
            animatorRight.SetFloat("Speed", vectorMov.sqrMagnitude);
        }

        rb.velocity = new Vector2(rb.velocity.x, movement * speed);

        var pos = transform.position;

        if (pos.y > boundY) {
            pos.y = boundY;
        } else if (pos.y < -boundY) {
            pos.y = -boundY;
        }
        transform.position = pos;
    }

    void playSound(float vertical, string sound) {
        if(vertical != 0 && !SoundManagerScript.audioSrc.isPlaying) {
            SoundManagerScript.PlaySound(sound);
        }
    }
}
