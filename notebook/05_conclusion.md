## a) Wie gut ließen sich ihre anfänglichen Ideen in das finale Spiel umsetzen?

Unsere Ideen waren fast alle umsetzbar, jedoch haben wir uns mit der Anzahl übernommen und haben es so leider zeitlich nicht geschafft, alle Ideen umzusetzen. Auch das erste arbeiten an einem "großen" Projekt in Unity hat dies erschwert, da mit mehr Erfahrung mit Unity auch stetiges refactoring nötig war.


## b) Wo sind sie stark abgewichen?

Aufgrund der unterschätzten Arbeit haben wir viele Features minimiert implementiert, die mit weiterer Zeit noch zum gewünschten Ziel erweitert werden könnten, aber leider auch viele Ideen verworfen wie Koop, Combos oder Pets.

## c) Wie haben die Elemente des Kurses (Entwicklungsplan, Prototyp, Playtesting etc.) das Fortkommen im Kurs begünstigt oder behindert?

Die Elemente des Kurses haben uns nicht behindert, sondern sogar sehr gut geleitet und unterstützt. Dank der ersten Aufgabe der Entwicklung von dem Spiel Pong haben wir schon einige Elemente von Unity erlernt und direkt anwenden können.
Von unsererem zuerst erstellten Entwicklungsplan mussten wir abweichen, da Abgaben in anderen Modulen in die quere kamen und den Zeitaufwand für die Implementierung der gewünschten Features unterschätzt haben. 

## d) Konnten Sie ihre Erwartungen umsetzen? Sind sie stolz auf das Spiel?

Wir sind sehr stolz auf unser Spiel. Rüchkblickend hätte man vieles besser machen können, aber das was wir erreicht haben, ohne vorherige Kentnisse ist sehr solide. Wir konnten jedenfalls alles Umsetzen was wir uns vorgenommen hatten, vielleicht nicht immer so schnell aber am Ende des projekts hatten wir eine gute Vorhergehensweise entwickelt für das Spiel.

## e) Hatten Sie genug Zeit?

Wir hatten genug Zeit. Wir haben uns etwas viel vorgenommen, deswegen hätte der polish sehr viel besser sein können aber alle Funktionen die man für das Spiel braucht haben wir umgesetzt.

## f) Was war die größte technische Schwierigkeit?

Was uns Schwierigkeiten bereitet hat war der das Miningsystem. Mit einem einzelnen objekt zu interagieren wenn es hunderte ähnliche objekte in der nähe gab, das hat das abbauen von Tiles etwas knifflig gemacht. Bomben und das herunterfallen hat aber trotzdem gut funktioniert.

## g) Sind sie mit dem Thema des Kurses klar gekommen? 

Das Thema Highscore war Super! Es ist ein thema das jedem der Spiele kennt aber auch in vielen anderen bereichen sehr vertraut, ein Thema zu dem man sich also leicht inspirieren lassen kann, aber auch im positiven sinne herausfordernd mal was neues draus zu machen. 

## h) Hat Ihnen die Arbeit mit dem Thema Spaß gemacht, oder hätten Sie lieber mehr Freiheit gehabt?

Das Thema 'Highscore' des Kurses hat uns persönlich gefallen, weil dieses mit unserem rogue-like adventure gepasst hat und wir damit anfangs schon auf viele Aspekte unseres Spiels gekommen sind.
Durch Aktionen wie das Abbauen von Ores oder das Besiegen von Gegnern sollte dazu angetrieben werden, einen möglichst hohen Score zu erhalten und dementsprechend auch ein gutes Ranking zu erzeugen. 


## i) Was würden Sie bei ihrem nächsten Spiel anders machen?

Wir würden uns besonders mit dem Leveldesign und mithilfe von kleinen Hinweisen auf das Prinzip "Don't tell but show" richten, um dem Spieler einen möglichst interessanten Einstieg in das Spiel ermöglichen zu können, während ihm eine Chance
zum selbstständigen Erkunden und Herausfinden gegeben wird, um im Spiel voranzukommen. Ein weiterer Ausblick für das nächste Spiel wäre ein gleicher Fokus sowohl auf den visuellen, künstlerischen Teil, als auch auf den auditiven Teil mit besseren Soundeffects und Soundtracks.

## j) Was war der größte Erfolg während des Projekts? War das Projekt ein Erfolg?

Dank unserer Playtester und Feedback des Dozenten, aber auch durch die gute Mitarbeit in der Gruppe, bekamen wir die Möglichkeit unser Spiel nach dem Alpha Release bzw. Playtesting auszubessern, was zum Erfolg des Projektes geführt hat. 
Wir konnten viele der Aspekte unserer Layer erfolgreich durchsetzen, unter Anderem ein funktionierendes Kampfsystem und einen persistenten Highscore.
Teil des großen Erfolges ist jedoch auch das Leveldesign, welches wir mithilfe der handgemachten Tilesets durchführen konnten.


## k) Mochten Sie Unity?

Da keiner aus unserer Gruppe Vorerfahrung mit Unity besaß, haben wir uns während des Projekts parallel mit Unity beschäftigt und konnten somit wertvolle Erfahrung sammeln.
Der Anfang gestaltete sich leicht herausfordernd, weil es eine neue Erfahrung gewesen ist eine Game Engine benutzen zu können.
Im Allgemeinen stehen wir zu Unity aber positiv.
