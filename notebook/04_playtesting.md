# Playtesting 
### Teilnehmer Profile
--- 
Spieler 1 

    Wichtige Merkmale 
    Alter: Junge Erwachsenen 
    Computerspielerfahrung: Viel 

Spieler 2 

    Wichtige Merkmale 
    Alter: Schüler 
    Computerspielerfahrung: Sehr Viel 

Spieler 3 

    Wichtige Merkmale 
    Alter: Junge Erwachsenen 
    Computerspielerfahrung: Wenig 

Spieler 4 

    Wichtige Merkmale 
    Alter: Junge Erwachsenen 
    Computerspielerfahrung: Sehr Viel 

Spieler 5 

    Wichtige Merkmale 
    Alter: Schüler 
    Computerspielerfahrung: Mittel 

### Organization 

    Die Spieler wurden gebeten das Spiel zu testen, dabei wurde vom Tester das verhalten und die Bemerkungen notiert.  
    Nach dem die Spieler das Spiel ausreichend erkundet haben wurden sie zu ihrer Erfahrung anhand der Fragen befragt.
### Spielerverhalten // Bemerkungen Der Spieler 
--- 

Spieler 1 

    - Spielstart 
    - Erkundet das Shop Level und geht direkt zu dem Leaderboard 
    - Der Level Wechsel aus dem Ersten Level war Verwirrend ein Großteil des Levels wird abgebaut. 
    - Wünscht sich ein Bombenindikator in der UI. 
    - Erst Später werden die Wände abgebaut. 
    - Wünscht sich verschiedenen Score für Verschiedene Kristalle. 
    - Wünscht sich Death Sounds (Verifizierung des AI Todes), Bluten. 
    - FM Sprite ist gelungen. 
    - 0 Score bei Level beginn war frustrierend. 
    - Wünscht sich das der UI Platz besser verwendet wird. 

Spieler 2 

    - Spielstart 
    - Versucht zu den Tunneln um den Start bereich zu kommen. 
    - Wirft Bomben in Wände 
    - Verwendet Jump in den Leveln um Löcher zu überqueren 
    - Tötet die Gegner sehr Schnell 
    - wirft Bomben und erkennt "Cooking" 
    - wünscht sich einen Munitions Counter,  
    - Abbau nach unten/oben ist schwierig wegen der Reichweite des Cursors. 
    - Finden der Ausgänge ist nicht intuitiv 
    - move-attack-jump Bug wird verwendet, der Character fällt nicht während Animationen. 
    - Spieler vermutet das es Stamina gibt? 

Spieler 3 

    - Spielstart 
    - Mausbewegung rausgefunden und klicken für Angriff 
    - Dann -> new game 
    - Erklärung nötig oder F1 (aber das war außerhalb des Screens) 
    - rausgefunden, dass auch Boden kaputt gehen kann. reingefallen 
    - Kampf intuitiv eingesetzt und verstanden 
    - Sprung etwas kniffelig, weil es dunkel ist 
    - durch Bug Level Reihenfolge durcheinander -> Neustart 

Spieler 4 

    - Spielstart 
    - Herausfinden der Controls im Shop Level 
    - new game 
    - Herumbuddeln an den Minenkanten des Levels (da Secrets path nicht offensichtlich war) 
    - Erklärung für Bestehen eines Levels -> find secret path für next level 
    - Abarbeiten der Levels, dabei Kampf + Abbauen von Ores intuitiv eingesetzt und verstanden 
    - ein paar mal gestorben (in ein Loch gefallen, aus Versehen eigenes Grab gebuddelt) 
    - insgesamt drei Spielanläufe bis zum letzten Level

Spieler 5 

    - Spielstart 
    - Controls schnell verstanden 
    - Möchte die Fackeln von dem Shop Level nehmen. 
    - Versucht den Shop und Shopkeeper zu versenken. 
    - Untersucht Das Leaderboard 
    - Untersucht das Level 1, erkennt einen Unterschied zwischen den Wall Tiles. 
    - Fast das ganze erste Level wird abgebaut. 
    - Combat war sehr intuitiv. 
    - Level Ausgänge zu finden war schwer. 
## Playtesting Fragen: 

    Falls Möglich Bittet um eine Wertung von 1 (Schlecht) bis 5 (Perfekt). 

    F1 - Ist die Einleitung in da Spiel gut verständlich, auch ohne explizites Tutorial(Sheet) ? 
    F2 - Wie umfangreich soll das Spiel deiner Meinung nach noch werden (mit Berücksichtigung auf restliche Zeit Bis zur finalen Abgabe) ? 
    F3 - Ist das Spiel für den Anfang herausfordernd/nicht zu einfach? 
    F4 - Regt das Spiel für einen möglichst hohen Highscore an? (zb fürs Kaufen von seltenen Items/competitiveness) ? 
    F5 - Bewertung der Optik von 1-5 
    F6 - Bewertung der Atmosphäre (Effekts/Ton/Animation) von 1 - 5 
    F7 - Diversität der Atmosphäre von 1 - 5 
    F8 - Diversität der Interaktionen/Kampf 1 - 5 
    F9 - Wünsche zu dem Shop Level 
    F10 - Wünsche für Dialog/Monolog 
## Antworten der Playtester 
--- 

Spieler 1 

    1. 3: Es war klar das es ein Mining Spiel ist, die Actions wurden beschrieben, Cursor war intuitiv. Score 0 war verwirrend, der High Score gab erst nach mehreren Spieldurchläufen Sinn. Ausgang der Levels ist schwer zu finden. Es war nicht klar das es mehrere Level gibt (Vom Anfang). 
    2. Wünsche: High Score sollte persistent sein, Health Icons, Informationen wie wertvoll Gems sind, Bomb Icons (Anzahl), Shop Integration, Inventar. 
    3. 2 (3: Nach Verbesserung): Steuerung ist aktuell herausfordernd, könnte mit Verbesserung Meditativ sein. 
    4. Ja 
    5. 5: Wünsche: Mehr UI, Mehr Ores 
    6. 3: Wünsche: Mehr Musik, andere Biotope 
    7. Wünsche: Andere Biotope, Seltene Level/Biotope 
    8. Wünsche: Getarnte Gegner, Sand Lion? 
    9. Wünsche: Unterschiedliche Bomben, Heilungstränke 
    10. Wünsche: Unterstützung als Einführung 

Spieler 2 

    1. 1-5:4 I think the game is relatively straight forward without a tutorial, what is unclear is how many bombs you have on hand 
    2. For completions sake, fix animation Bugs, Easter eggs in a corner of a room under a wall. Incorporate Starting Area Tunnels. Audio Delay on Bombs 
    3. 1-5:2 game is not very dificult for me, for people who struggle with platformers maybe. 
    4. 1-5:3 score reset on each level, total score would be better. Golden Pickaxe for score, skins, visuelle anreize 
    5. 1-5:4 Visuals were good pixelart was done very well. 
    6. 1-5:2-3 Due to a Lack of Music, more sfx 
    7. 1-5:3-4 A few different color schemes, Different Lighting options, glowing mushrooms from skyrim 
    8. 1-5:3 Combat seems relatively basic, combat doesn't necesarily need to be more complicated 
    9. Shop lvl: buying Remotely Detonated Stuff, Different Colored Headlamp, verschieden Buffs - Drinks, Health, Speed, Bait//Lures 
    10. Dialog/Monologue: Player Model reactions, pickup, reaction to monsters, Hints in Dialogue. 

Spieler 3 

    1. Nein, hatte keine Ahnung welche Knöpfe benutzt werden müssen 
    2. Mehr Level 
    3. Nein, es geht. Nach zwei drei Versuchen geht es gut. 
    4. Nein, bisher kann man ja keine kaufen 
    5. 5! 
    6. 4 
    7. 2 
    8. 2 
    9. Ein Bogen oder andere Fernkampfwaffe 
    10. Tipps fürs Spiel vom Händler? 

Spieler 4 

    1. Nein, die Controls sind etwas verwirrend(Mine + attack animations sehen ähnlich aus, machen aber verschiedene Dinge) HUD mit Lebensanzeige war nicht eindeutig genug ABER Verlauf der Level (increasing difficulty) war gut 
    2. Mehr Level, bessere Stages -> Bosslevel, bessere Gegner 
    3. Nein, da Gegner geonehittet werden vom Player + nur zwei Gegnertypen vorhanden sind  Verbesserung: z.B. selbst explosive enemies, enemies mit festen Pattern/moveset, Varianten(Tollwut bat, poison bug) 
    4. Nein, da Items z.B. noch unklar sind 
    5. 4 Points, nur HUD war unklar  
    6. 3.5 - 4 Points, background music / sfx for more atmosphere needed 
    7. 1 Points, da Level im Grunde gleich aussehen 
    8. 0-1 Points, aufgrund der zu leichten Gegner und nicht verschieden genug  
    9. Items -> Tinkturen, weapons, undertale like music 
    10. Nicht aktiver Shopkeeper -> sleeping sound or text box with ZZZZzzzZ, no monologue for player necessary 

Spieler 5 

    1. 2 - Außer der Kontrolle Übersicht gibt es keine Hilfestellungen. 
    2. Ein paar Variationen von Gegnern und Leveln wären gut. 
    3. 3 - Man muss planen wo man Bomben verwendet, sie können Level sehr schnell schwer machen. 
    4. 2 - Nein da man noch nicht weiß was man kaufen könnte. 
    5. 3 - Die Sprites sind sehr gut, Variationen davon wären gut 
    6. 3 - Mehr SFX würden einen großen Unterschied machen 
    7. 2 - Zu wenige unterschiede Zwischen den leveln. 
    8. 2 - Gegner waren sehr einfach. Viele Gegner konnten mit einem Schlag getötet werden. 
    9. Items verbesserte Werkzeuge und Consumables würden Sinn ergeben 
    10. Hilfe vom Shopkeeper wäre Hilfreich 

## Erkentnisse 

    1. Für viele Spieler ist der Einstieg schwer, der Shopkeeper sollte genutzt werden um Hilfestellungen zu geben. 
    2. Alle Spieler wünschen sich eine UI mit mehr Informationen. 
    3. Die Kontrolle der Mining Interaktion sind schwierig. 
    3. Viele Spieler wünschen sich den Shop als Informationsquelle (Preise für Erze), aber auch für andere Items/Tools 
    4. Viele Spieler bauen einen großen Teil vom Level ab, erwarten auch mehr dafür (Entdeckung). 
    5. Für die meisten Spieler ist der Combat Teil nicht Anspruchsvoll. 
    6. Manche Spieler wünschen sich Ausgefallenere Gegner, aber auch bessere Waffen. 
    7. Manche Spieler wünschen sich mehr Environment Sprites und Lighting um verschiedene Atmosphären zu schaffen. 
    8. SFX würden das Spiel am meisten verbessern. 
    9. Mehr Utility aus dem Shop. 
## Mögliche Lösungen fürs Release: 

    1. Umsetzung von Dialogen/Monologen vom Player und Shopkeeper um Das Spiel zu Erklären. 
    2. Die UI Fläche nutzen für Counter/ Health bar. 
    3. Die Mining Interaktion Nochmal Untersuchen. 
    4. Das Dialogsystem verwenden um Informationen über den Shop zu geben. 
    5. Gegner Variationen, Gepanzert, andere Angriffe (Spitter?), Worms, Sand Lions 
    6. Büchsen, Armbrust, Lures etc. 
    7. Mehr Diverse Lichtquellen. 
    8. Hintergrund Musik etc. 
    9. Consumeables etc. 