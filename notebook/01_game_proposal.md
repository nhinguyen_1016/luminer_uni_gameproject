# Titel: Luminer

__Informationen entnehmen Sie bitte dem Übungsblatt!__

## 1.Spielbeschreibung 

    Konzept:

    Das Ziel des Spielers sollte es sein einen möglichst hohen Score innerhalb eines Runs durch das abschließen von so vielen Leveln wie nur möglich zu erreichen. Score kann aus verschiedenen Quellen gewonnen werden, hauptsächlich durch das abbauen von Ressourcen in der Mine, wenn er Gegner besiegt, und das Level schnell abschließt da spätere Level schwerer aber auch lukrativer sein sollen. Der Spieler hat die Chance zu entscheiden was der beste Weg ist innerhalb eines Levels viel Score zu gewinnen und Strategien zu entwickeln die zu seinem Spielstil passen. Was an Score noch interessant sein soll ist das er bei Händlern und in Shops verwendet werden kann um Items zu kaufen. Der Sinn dahinter ist das diese Items dem Spieler zum teil permanente aber auch einmalige vorteile geben können die ihm ermöglichen level schneller abzuschließen und dadurch eine möglichst hohen score, der Spieler aber abwägen muss ob die kosten sich für den vorteil lohnen, was sehr Zentral zu Luminer ist da jeder Tod den Spieler Score Kostet um sich wiederzubeleben.

    Inspirationen:
    
    Für Luminer haben wir aus vielen verschiedenen quellen Inspiration genommen. Das Leveldesign und auch die Kameraperspektive kommen aus Stardew Valley wo es mehrere Minen gibt die der Spieler entdecken kann um an essentielle ressourcen zu Gelangen. Und auch Deep Rock Galactic, wo man als Zwerge unterirdisch bestimmte Ziele bewältigen muss während man von wellen von Gegnern angegriffen wird. 

![](./images/stardewmine.jpg)
![](./images/EA-008_we_got_company.png)
![Gif Test](./images/flare.gif)

    Die Atmosphäre ist aber nicht kontemporär, sondern eher an ein Post/Apokalyptisches 19 Jahrhundert angelegt wie in Frostpunkt aber auch Hunt: Showdown. 

![](./images/h1.png)
![](./images/original.jpg)

    Der fokus auf den Wiederspielwert und auf status Effekte von Luminer kommt aus einem gesamten Genre, nämlich dem sogenannten Roguelike, das sind Spiele wie Slay the Spire, Darkest Dungeon. 

    Das Highscore System von Luminer ist auch an das von Cuphead inspiriert, denn basierend darauf, wie schnell der Spieler mit seinen persönlichen Stärken ein Level erfolgreich abschließt, bekommt dieser einen jeweiligen Highscore/ Grade per Level completion.

    Das Hack and Slash Element nimmt Inspiration von den Spielen von Supergiant Games wie Hades oder Bastion.

    Der Art Style ist hauptsächlich von den Spielen Octopath Traveler oder Fire Emblem Awakening inspiriert worden, die beide einen einzigartigen Pixel Art Style anbieten

![](./images/Artstyle-Octopath-Traveler.jpg)

    Die Inspiration zur Levelgröße und 2.5 Dimensionalen Kamera kam von dem Spiel Blazing Beaks. Hier ist die Kamera und Levelgröße zumindest für die ersten Stages fix auf die Bildschirmgröße limitiert und damit die Kamera statisch, was für eine sehr gute Übersicht sorgt. Level sind damit auch nicht zu groß und werden dadurch kompakt und das Spiel damit schnelllebig.

![](./images/blazing_beaks_screenshot.jpg)

    Weitere Inspirationen sind Stardew Valley und das Original Harvest Moon. In diesen Spielen gibt es als Nebentätigkeit die Möglichkeit in Höhlen zu gehen und dort Ressourcen zu schürfen.

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](https://www.youtube.com/watch?v=9kZTttG7Tbs)
    
    Storyline Lore:
    
    Der Spieler ist ein Luminer. Seit dem Phänomen das vor 20 jahren das ganze leben unter die Oberfläche gezwungen hat, sind es die Luminer die kleine Siedlungen und Dörfer am leben halten. Licht ist seltene ware und das instandhalten und die subterrane Landwirtschaft fordert viel von diesen kleinen Gruppierungen. Nur die Mutigsten und erfahrensten Entdecker werden wirklich zu Luminer, die Bergleute die Tief in verlassene Minen vordringen und sich gegen die unnatürlichen Wesen behaupten die unmittelbar um die Glimmsteine existieren. Denn die Glimmsteine sind das ziel eines jeden Luminers. Erst seit dem Phänomen gibt es sie in verschiedenen Arten und abgesehen das sie lange und Hell leuchten was es den Aufwand schon wert macht sie zu suchen, sind sie unverzichtbar für das leben der Unterirdischen dörfer. Ein paar Forscher haben wege gefunden sie in maschinen und aber auch der Landwirtschaft einzubinden um neue pflanzen und pilze zu züchten. Glimmstein ist deswegen unglaublich wertvoll und die wenigen Luminer die es gibt sind die Helden ihrer Heimat.

    Bezug zum Kursthema:

    Die erste Idee zum Kursthema "Highscore" war, dass man nicht einfach einen konventionellen Highscore einbaut, der eher nachträglich hinzugefügt worden ist, sondern versucht, diesen in das Spielgeschehen zu integrieren, indem der Score als Währung genutzt wird.
    So kann der Spieler in Luminer für das bestreiten von Ebenen in der Mine Punkte erhalten sowie auch für das Bekämpfen von Gegnern oder durch Abbau von Ressourcen.
    Diese/r Währung/Score kann dann in Shops zwischen den Ebenen mit Items getauscht werden, mit denen man ein Vorteil in tieferen Ebenen erhalten könnte, wie zum Beispiel eine Bombe, die zum Sprengen von mehreren Steinblöcken oder stärkeren Gegnern genutzt werden kann.
    Spieler mit besserer Erfahrung könnten gezielter Items kaufen oder sogar darauf verzichten und so einen höheren Score erarbeiten als Anfänger.

    Gameplay:

    Der Spieler findet sich in einer Höhle wieder und arbeitet sich von Ebene zu Ebene tiefer in die Erde. Um von einer Ebene zur nächsten zu gelangen, muss er alle Gegner besiegen, bevor sich die Luke zur nächsten Ebene öffnet.
    Dabei kann er Gegner durch seine Gegenstände verletzen, aber auch in Löcher stoßen, um sie zu bezwingen. Einzelne Terrainblöcke (wie Stein, Erde, Granit, etc.), können durch den Spieler zerstört werden um so Ressourcen zu finden oder aber Gegner zu entkommen und auch der Boden kann gegebenenfalls mit mehreren Schlägen geöffnet werden um diese Löcher taktisch nutzen zu können.


## 2.Technische Elemente

    Luminer ist ein 2D Rogue-like Spiel, welches den Spieler durch verschiedenste Minenlevel in der Top-Down View begleitet. Die generelle Pixel Sprite Size sowohl von den Charakteren, als auch der Level sind (mindestens) 32 Pixel groß, um den Spieler durch eine komplexe Welt unterhalb der Erdoberfläche zu führen und auch um Effekte wie Lighting besser darstellen zu können, ohne von einer kleineren Größe limitiert zu werden. Das Gameplay soll jedoch im Hauptfokus bleiben. Dabei wird es je 3-4 Sprites für eine typische Animation, wie das Angreifen oder Abbauen geben.

    Luminer soll im Single Player Mode spielbar sein, aber auch die Möglichkeit geben, sich lokal mit einem anderen Kameraden zusammen auf das Abenteuer zu begeben.
    Gespielt wird mit einem XBox Controller/ Tastatur und Maus(?).
    Das Spieltempo soll möglichst schnell sein, um sich von Level zu Level durchzuschlagen und dabei den Schwierigkeitsgrad im weiteren Verlauf zu steigern. 
    Der Spieler ist mit verschiedenen Angriffen dazu in der Lage, sich ihm im Wege stehende Gegner schnell zu beseitigen, um zum Beispiel besonders seltene Mineralien einzusammeln.


## 3. "Big Idea"

    Luminer begleitet den Spieler durch ein einzigartiges Spielerlebnis in verschiedenste Minenlevel, um wertvolle Mineralien für sein Überleben unterhalb der nun unbewohnbaren Erdoberfläche zu sammeln. Dabei werden dem Spieler viele Kreaturen im Weg stehen um ihn an sein Weiterkommen zu hindern.
    Das Spiel implementiert Elemente aus dem Rogue-like Genre und gibt dem Spieler zum Beispiel Waffen und Angriffe zur Verfügung, um sich so gut wie möglich durch gefährliche Monstermassen durchzukämpfen. Im Falle eines Spielertodes verliert er als Strafe einen Teil seiner Währung, gesammelt durch seinen Highscore, und schließt im schlechten Fall mit einem schlechten Highscore ab. Auch werden ihm natürlich Items in seinem Inventar zur Verfügung gestellt, um ihm eine kleine Hilfe durch die Level zu geben.

    Luminer ist für jeden Rogue-like Fan, aber auch Pixel Art Liebhaber gedacht, da das Spiel auch ein besonderes Level Design des im 19. Jahrhundert spielenden Minen-Settings anbietet, durch die sich der Spieler schlagen muss. Der Spieler wird aber auch einen Rückzugsort vor und nach einem Level haben, nämlich ein Camp mit Lager und Itemshop, in welches er sich für eine Pause niederlassen oder für sein nächstes, bevorstehendes Abenteuer vorbereiten kann.

## 4. Entwicklungszeitplan
---

### **Alpha**
---
Layer 1 - Funktionales Minimum:
---
    Level Design  
    Charachter / Visual Design 
    Start Item
    Base Level
    Mob
    Abbaubare Elemente
    Level Verlassen
    Inventar
    Leben 
    Highscore
    Charachter Controls

Layer 2 - Minimales Ziel: 
---    
    Kampfsystem
    Charachter Animations
    User Interface
    Combos
    Damage Types & Effects
    Mehrere Levels
    Shops
    Mehrere Mobs
    Loot
    Light & LOS
    Story
    Base Sound Design

### **Playtesting**
--- 
Layer 3 - Ziel:
---
    Playtesting
    Advanced Animation
    Levels Overhaul
    Bughunting and implementing Feedback
    Biome
    Pets & Companions
    Grade per Level
    Skills (Leveling)    

### **Release**
---
Layer 4 - Wunschziel:
---

    Quests - Wiederkehrende NPC's
    Charachter Creation
    Klassen
    Mutatoren
    Mini Bosse
    Camp
    Co-op Vs

Layer 5 - Extras
---

    Open World
    Neutral Charachters
    Multiplayer
    4 player online co-op
    crafting
### **Initialer Entwicklungszeitplan**
---
![](./images/Gantt.jpg)

Der Entwicklungszeitplan unterliegt der Teamaufteilung.

## 5. Teamaufteilung

    GameDesigner - Alle
    Produzent - Tobin
    Programmierer - Lead Georg
    Künstler - Lead Artest Nhi
    Audio - Lead Audio Nhi
    Level Design - Lead Tobin
    Qualitätsicherung - Lead Georg