1.  Bitte ändern Sie die Ordnerstruktur nicht! 

2.  Bitte fügen Sie in jeden Spieleordner die notwendigen *.gitignore* und *.gitattributes* ein!

3.  Bitte nutzen Sie die vorgegebenen *.md* Dateien im Ordner *notebook* um das Projekttagebuch zu füllen und nennen Sie diese __nicht__ um oder fügen weitere Dateien hinzu oder ähnliches. 

	-- game

	-- -- Breakout

	-- -- YourGameTitleHere

	-- notebook

