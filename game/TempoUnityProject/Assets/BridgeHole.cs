using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeHole : MonoBehaviour
{
    public GameObject completeHole;
    public Health H;

    private void OnTriggerStay2D(Collider2D other)
    {
        if(H.GetHealth() < 3)
        {
            if (other.CompareTag("Player"))
            {
                var player = other.GetComponent<PlayerMovement>();
                if (!player.isJumping)
                {
                    player.FallingInHole();
                }
            }
            if (other.CompareTag("Enemy"))
            {
                var enemy = other.GetComponent<EnemyController>();
                if (enemy.canFall)
                {
                    enemy.FallingInHole();
                }

            }
        }
    }

    public void SwitchHolePrefab()
    {
        if (!(this.gameObject == completeHole))
        {
            Instantiate(completeHole, this.transform.position, this.transform.rotation);
            Destroy(this.gameObject);
        }
    }


}
