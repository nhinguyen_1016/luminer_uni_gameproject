using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpUIScript : MonoBehaviour
{
    private Canvas cs;
    //private Canvas cs2;

    // Start is called before the first frame update
    void Start()
    {
        cs = GetComponentInChildren<Canvas>();
        //cs2 = GetComponentInChildren<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F1))
        {
            cs.enabled = !cs.enabled;
            //cs2.enabled = !cs2.enabled;
        }
    }
}
