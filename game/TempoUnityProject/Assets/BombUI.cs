using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BombUI : MonoBehaviour
{
    [SerializeField] GameObject b1;
    [SerializeField] GameObject b2;
    [SerializeField] GameObject b3;

    private GameObject player;
    PlayerCombat pc;
    private double counter;
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        pc = player.GetComponent<PlayerCombat>();
        counter = pc.getBombAmount();

        if (counter < 3)
        {
            b3.GetComponent<Image>().enabled = false;
        }

        if (counter < 2)
        {
            b2.GetComponent<Image>().enabled = false;
        }

        if (counter < 1)
        {
            b1.GetComponent<Image>().enabled = false;
        }
    }

    private void Update()
    {
        runCounter();
    }

    private void runCounter()
    {
        if (counter != pc.getBombAmount())
        {
            counter = pc.getBombAmount();
            Debug.Log("This is a counter");

            if (counter < 3)
            {
                b3.GetComponent<Image>().enabled = false;
            }

            if (counter < 2)
            {
                b2.GetComponent<Image>().enabled = false;
            }

            if (counter < 1)
            {
                b1.GetComponent<Image>().enabled = false;
            }
        }
    }
}
