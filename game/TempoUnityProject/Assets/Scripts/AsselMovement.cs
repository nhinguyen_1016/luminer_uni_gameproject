using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsselMovement : MonoBehaviour
{

    public Animator animator;

    float horizontalMove = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Will control the movement and animation.
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

    }
}
