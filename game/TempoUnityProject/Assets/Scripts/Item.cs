using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public GameObject bomb;
    public GameObject explosion;
    PlayerController player;
    // Start is called before the first frame update
    Vector3 itemPostion;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Use(string itemName, float fuse)
    {
        switch (itemName)
        {
            case "bomb":
                Bomb(fuse);
                break;
            default:
                Debug.Log("error");
                break;
        }
    }

    IEnumerator Explode(GameObject item, float delay, float explosionRadius)
    {
        yield return new WaitForSeconds(delay);
        itemPostion = item.transform.position;
        int wrld = 1 << LayerMask.NameToLayer("WorldTiles");
        int ent = 1 << LayerMask.NameToLayer("DamageBox");
        int mask = wrld | ent;
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(itemPostion, explosionRadius, mask);
        Debug.Log("bomb explodes hitting " + hitColliders.Length + " objects");
        foreach(var hitCollider in hitColliders)
        {
            GameObject obj = hitCollider.gameObject;
            obj.GetComponent<Health>().GiveDamage(2);
            FindObjectOfType<AudioManager>().Play("Bomb_Explosion");

        }

        GameObject replObj = Instantiate(explosion);
        replObj.transform.position = item.transform.position;
        Destroy(item);
        Destroy(replObj, 2f);
    }

    private void Bomb(float fuse)
    {
        float cookingTime = 4 - fuse;
        float explosionRadius = 2;
        Vector3 worldMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = (Vector2)((worldMousePos - transform.position));
        direction.Normalize();
        // Creates the bomb locally
        GameObject item = (GameObject)Instantiate(
                                bomb,
                                transform.position + (Vector3)(direction * 0.5f),
                                Quaternion.identity);
        // Adds velocity to the bomb
        item.GetComponent<Rigidbody2D>().velocity = direction * 10;

        StartCoroutine(Explode(item, cookingTime, explosionRadius));
    }
}
