using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatistics
{
    public float HP;
    public float Ammo;
    public float XP;

    public int Score;

    public double health;
    public double healthPot;
    public double bombs;

    public PlayerStatistics()
    {
        Score = 0;
        health = 5;
        healthPot = 3;
        bombs = 100;
    }
}
