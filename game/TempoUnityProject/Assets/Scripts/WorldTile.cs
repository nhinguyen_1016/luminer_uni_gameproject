using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(SpriteRenderer))]

public class WorldTile : MonoBehaviour
{
    ParticleSystem particle;
    PlayerController player;
    public GameObject replacementObject;
    public Sprite intact;
    public Sprite partiallyBroken;
    public Sprite almostBroken;

    private SpriteRenderer sr;
    private bool isIntact;

    private Health health;

    private double fullHealth;
    double healthInPercent;

    ScoreManager sm;

    public PlayerStatistics localPlayerData = new PlayerStatistics();

    public void Interact(bool isOre)
    {
        health.GiveDamage(1);
        player.MineInteraction();
        particle = GetComponentInChildren<ParticleSystem>();
        particle.Play();
        if (isOre)
        {
            int randomInt = UnityEngine.Random.Range(20, 50);
            Debug.Log(randomInt);   
            DropChunks();
            sm.AddScore(randomInt);
        };
    }

    private void Start() 
    {
        sm = GameObject.Find("UI").GetComponent<ScoreManager>();

        if(GetComponent<Health>() == null) {
            fullHealth = int.MaxValue;
        } else {
            health = GetComponent<Health>();
            fullHealth = health.GetHealth();
        }

        try
        {
        player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
        }
        catch(NullReferenceException err){
            //TODO:
        }
        sr = GetComponent<SpriteRenderer>();
        sr.sprite = intact;
    }

    void FixedUpdate()
    {
        try
        {
            healthInPercent = health.GetHealth() * 100 / fullHealth;
            if(isIntact) sr.sprite = intact;
            if(healthInPercent <= 75)
            {
                sr.sprite = partiallyBroken;
            }
            if(healthInPercent <= 50)
            {
                sr.sprite = almostBroken;
            }
            if(healthInPercent <= 0)
            {
                isIntact = false;
                GameObject replObj = Instantiate(replacementObject);
                replObj.transform.position = this.transform.position;
                Destroy(this.gameObject);

            }      
        }
        catch (System.Exception)
        {
            
        } 
    }

    void DropChunks()
    {

    }

    //Save data to global control   
    public void SavePlayer()
    {
        Debug.Log("saving score:" + localPlayerData.Score);
        GameDirector.Instance.savedPlayerData = localPlayerData;        
    }
}

