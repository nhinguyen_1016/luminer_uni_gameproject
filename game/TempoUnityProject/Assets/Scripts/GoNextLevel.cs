using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoNextLevel : MonoBehaviour
{
    public ScoreManager sm;

    public string nextLevel;

    private void Start() {
        GameObject gd = GameObject.Find("NextLevelInfo");
        this.nextLevel = gd.GetComponent<NextLevelInfo>().nextLevel;    
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player"))
        {
            SceneManager.LoadScene(nextLevel);
        }
    }
}