using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public double health;
    public bool dead;
    // Start is called before the first frame update
    void Start()
    { 
        
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0)
        {
            dead = true;
        }
    }

    public void GiveDamage(double damage)
    {
        health = health - damage;

        if (health <= 0)
        {
            Debug.Log("Health is below 0: " + health);
            Rigidbody2D rb2 = GetComponentInParent<Rigidbody2D>();
            rb2.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }

    public void SetHealth(double health)
    {
        this.health = health;
    }

    public double GetHealth()
    {
        return health;
    }
}
