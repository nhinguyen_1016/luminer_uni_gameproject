using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
    public GameObject completeHole;

    private void OnTriggerStay2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            var player = other.GetComponent<PlayerMovement>();
            if(!player.isJumping) {
                player.FallingInHole();
            }
        }
        if(other.CompareTag("Enemy"))
        {
            var enemy = other.GetComponent<EnemyController>();
            if (enemy.canFall)
            {
                enemy.FallingInHole();
            }
            
        }

        BridgeHole bridgeHole = other.gameObject.GetComponent<BridgeHole>();
        if(other.CompareTag("Hole") || bridgeHole != null)
        {
            var hole = other.transform.parent.gameObject.GetComponent<Hole>();
            hole.SwitchHolePrefab();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        BridgeHole bridgeHole = other.gameObject.GetComponent<BridgeHole>();
        if(other.CompareTag("Hole") || bridgeHole != null)
        {
            var hole = other.transform.parent.gameObject.GetComponent<Hole>();
            hole.SwitchHolePrefab();
        }
    }
    public void SwitchHolePrefab()
    {
        if(!(this.gameObject == completeHole)) {
            Instantiate(completeHole, this.transform.position, this.transform.rotation);
            Destroy(this.gameObject);
        }
    }

    
}
