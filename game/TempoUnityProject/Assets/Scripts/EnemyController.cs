using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public int maxHealth = 1;
    int currentHealth;
    public Animator animator;

    public bool canFall = true;
    Health heart;
    double health;
    bool isFalling;
    ScoreManager score;

    bool isDead;
    
    // Start is called before the first frame update
    void Start()
    {
        score = GameObject.Find("UI").GetComponentInChildren<ScoreManager>();
        isDead = false;
        isFalling = false;
        heart = GetComponentInChildren<Health>();
        heart.SetHealth(maxHealth);
        health = heart.GetHealth();
    }

    private void FixedUpdate()
    {
        health = heart.GetHealth();

        if (isFalling)
        {
            Debug.Log("enemy falling");
            animator.SetBool("isFalling", true);
            score.AddScore(300);
            StartCoroutine(KillObject());
        }
        
        
        if (health <= 0 && !isFalling && !isDead)
        {
            Debug.Log("enemy died!");
            animator.SetBool("isDead", true);
            gameObject.layer = 10;
            isDead = true;
            canFall = true;
            score.AddScore(300);
        }
    }

    public void FallingInHole()
    {
        Debug.Log("enemy falling in hole");
        isFalling = true;
    }

    IEnumerator KillObject()
    {
        yield return new WaitForSeconds(0.4f);
        Destroy(gameObject);
    }
}
