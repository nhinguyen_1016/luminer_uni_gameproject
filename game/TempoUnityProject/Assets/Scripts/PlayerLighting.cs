using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLighting : MonoBehaviour
{

    Vector2 moveDirection;
    public GameObject playerhead;
    private float angle = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");
        moveDirection = new Vector2(moveX, moveY);
        if (moveDirection.magnitude > 0)
        {
            angle = Mathf.Atan2(moveY, moveX) * Mathf.Rad2Deg;
            //Debug.Log("angle: " + angle);
            playerhead.transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        } else {
      
        }   
    }
}
