using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{ 
    public float miningRadius;
    private Vector2 boxSize;
    public SpriteRenderer playerSprite;
    public Animator animator;
    private Mining miningPoint;
    private Vector2 attackPosition;
    private PlayerMovement playermov;
    public int minedAmount;
    private int ore1 = 0;
    public bool isDead;
    public Health health;
    private double hp;

    private void Start() {
        miningPoint = this.GetComponentInChildren<Mining>();
        Physics2D.queriesStartInColliders = true;
        playermov = GetComponent<PlayerMovement>();
        health = GetComponentInChildren<Health>();
        playerSprite = GetComponentInChildren<SpriteRenderer>();
        hp = GameDirector.Instance.savedPlayerData.health;
        health.SetHealth(hp);
    }
    private void Update() {

        if(Input.GetKeyDown(KeyCode.E))
        {
            miningPoint.Mine(miningRadius);
            MineInteraction();
        }
        
        if (Input.GetButtonDown("Submit") && playermov.isFalling)
        {
            Respawning();
        }
    }

    private void FixedUpdate() {
        hp = health.GetHealth();
        if(hp <= 0.0)
        {
            Debug.Log("HES DEAD JIM");
            SceneManager.LoadScene("Deadscreen");
        }

        GameDirector.Instance.savedPlayerData.health = health.GetHealth();

    }
    public void MineInteraction()
    {
        FindObjectOfType<AudioManager>().Play("Mine_ore");
        ore1++;
        animator.SetTrigger("playerMine");

    }

    public void Respawning()
    {
        Debug.Log("respawning");
        var spawnpoint = GameObject.Find("Spawn");
        playermov.canControl = true;
        playermov.moveSpeed = 5;
        playermov.isFalling = false;
        this.transform.position = spawnpoint.transform.position;
        playerSprite.sortingOrder = 0;
        health.GiveDamage(1);
    }
}   
