using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed;
    public Rigidbody2D rb;
    public Animator animator;
    Vector2 moveDirection;

    public bool isJumping = false;
    public bool isFalling = false;
    public bool canControl = true;

    public float moveX;
    public float moveY;
    

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInputs();
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Player_Jump") &&
        animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.7f)
        {
                 isJumping = false;
        }
    }

    void FixedUpdate()
    {
        if(!canControl) moveSpeed = 0;
        Move();
    }

    void ProcessInputs()
    {
        moveX = Input.GetAxisRaw("Horizontal");
        moveY = Input.GetAxisRaw("Vertical");
        float moving = Mathf.Abs(moveX) + Mathf.Abs(moveY);

        if (moveX > 0.01) GetComponentInChildren<SpriteRenderer>().flipX = true;
        if (moveX < -0.01) GetComponentInChildren<SpriteRenderer>().flipX = false;

        animator.SetFloat("Speed", moving);
        moveDirection = new Vector2(moveX, moveY);

        if (Input.GetButtonDown("Jump"))
        {
            Debug.Log("player jumps");
            Debug.Log("player falls: " + isFalling);
            FindObjectOfType<AudioManager>().Play("Jump");
            isJumping = true;
        }
        animator.SetBool("isJumping", isJumping);
        animator.SetBool("isFalling", isFalling);
    }

    void Move()
    {
        rb.velocity = new Vector2(moveDirection.x * moveSpeed, moveDirection.y * moveSpeed);

    }

    public void FallingInHole()
    {
        Debug.Log("player falling in hole");
        isFalling = true;
        canControl = false;
    }
}
