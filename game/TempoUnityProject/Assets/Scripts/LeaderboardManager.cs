using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class LeaderboardManager : MonoBehaviour
{
    public Collider2D col;

    string name;
    public GameObject inputField;

    int score;
    public GameObject textDisplay;

    public GameObject leaderboard;

    public static SortedList<int, string> highscore = new SortedList<int, string>();

    public ScoreManager sm;

    // Start is called before the first frame update
    void Start()
    {
        if(GameObject.Find("PlayerObject")) {
            leaderboard.GetComponent<Canvas>().enabled = false;
        }
        sm = GameObject.Find("UI").GetComponent<ScoreManager>();
        score = sm.score;
        highscore.Add(1337, "Georg");
        highscore.Add(29489, "Nhi");
        highscore.Add(18347, "Tobin");
    }

    // Update is called once per frame
    void Update()
    {
        textDisplay.GetComponent<Text>().text = createList();
    }

    public void saveScore()
    {
        name = inputField.GetComponent<Text>().text;
        highscore.Add(score, name);
    }

    public string createList()
    {
        string list = "";

        foreach(KeyValuePair<int, string> entry in highscore.Reverse())
        {
            list = list + System.Environment.NewLine + entry.Value + " - " + entry.Key;
        }
        return list;
    }
}
