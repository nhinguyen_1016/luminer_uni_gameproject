using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeHoleDetect : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if(other.CompareTag("Hole"))
        {
            var hole = other.transform.parent.gameObject.GetComponent<Hole>();
            hole.SwitchHolePrefab();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Hole"))
        {
            var hole = other.transform.parent.gameObject.GetComponent<Hole>();
            hole.SwitchHolePrefab();
        }
    }

}
