using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text scoreText;
    public int score;
    // Start is called before the first frame update
    void Start()
    {
        score = GameDirector.Instance.savedPlayerData.Score;
    }

    // Update is called once per frame
    void Update()
    {
        score = GameDirector.Instance.savedPlayerData.Score;
        scoreText.text = score.ToString();
        score = int.Parse(scoreText.text);
    }

    public void AddScore(int points)
    {
        GameDirector.Instance.savedPlayerData.Score += points;
    }
}
