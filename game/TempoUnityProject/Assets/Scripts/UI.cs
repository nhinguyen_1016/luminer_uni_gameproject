using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class UI : MonoBehaviour
{
    public Canvas ui;
    double health;

    public Sprite imageFiveHealth;
    public Sprite imageFourHealth;
    public Sprite imageThreeHealth;
    public Sprite imageTwoHealth;
    public Sprite imageOneHealth;
    
    public Image playerAvatar;
    private Health playerHealth;
    // Start is called before the first frame update
    void Start()
    {
        try
        {
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Health>();
        }
        catch(NullReferenceException err){
            //TODO:
        }
    }

    private void FixedUpdate()
    {
        try
        {
        health = playerHealth.GetHealth();
        }
        catch(NullReferenceException e) {
            health = 0;
        }
        ChangeAvatar();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeAvatar()
    {
        switch (health)
        {
            case 0:
                playerAvatar.sprite = imageOneHealth;
                break;
            case 1:
                playerAvatar.sprite = imageOneHealth;
                break;
            case 2:
                playerAvatar.sprite = imageTwoHealth;
                break;
            case 3:
                playerAvatar.sprite = imageThreeHealth;
                break;
            case 4:
                playerAvatar.sprite = imageFourHealth;
                break;
            case 5:
                playerAvatar.sprite = imageFiveHealth;
                break;
            default:
                Debug.Log("Error");
                break;
        }
    }
}
