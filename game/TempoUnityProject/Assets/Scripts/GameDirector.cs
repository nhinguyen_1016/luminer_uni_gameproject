using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    public static GameDirector Instance;
    public ScoreManager sm;
    public PlayerController player;
    public Text score;
    public int scoreCalc;
    public double levelcount;

    public PlayerStatistics savedPlayerData = new PlayerStatistics();
    
    public string nextLevel;
    // Start is called before the first frame update
    void Start()
    {
        score = GameObject.Find("UI").GetComponentInChildren<Text>();
    }

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        score = GameObject.Find("UI").GetComponentInChildren<Text>();
        score.text = scoreCalc.ToString();
        scoreCalc = int.Parse(score.text);
        
    }
}
