using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    public Animator animator;
    public GameObject attackPoint;

    PlayerController player;

    Rigidbody2D attackPointRb;
    public float attackRadius = 0.5f;
    public float attackRange;
    public LayerMask enemyLayers;
    public int attackDamage = 40;
    PlayerMovement movement;
    float moveX;
    float moveY;
    Vector3 mousePos;
    Vector3 playerPos;

    public double Bombs;
    bool amThrowing = false;

    public double healthPot;

    float timeCurrent;
    float timeAtButtonDown;
    float timeAtButtonUp;
    float fuse;

    private void Start() {
        player = GetComponent<PlayerController>();
        movement = GetComponent<PlayerMovement>();
        attackPointRb = attackPoint.GetComponent<Rigidbody2D>();

        Bombs = GameDirector.Instance.savedPlayerData.bombs;
        healthPot = GameDirector.Instance.savedPlayerData.healthPot;
    }
    void Update()
    {
        timeCurrent = Time.fixedTime;

        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        playerPos = gameObject.GetComponentInChildren<CapsuleCollider2D>().transform.position;
        Vector3 playerToCursor = mousePos - playerPos;
        Vector3 dir = playerToCursor.normalized;
        Vector3 cursorVector = dir * attackRange;
        Vector3 finalPos = playerPos + cursorVector;

        attackPointRb.MovePosition(finalPos);


        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            FindObjectOfType<AudioManager>().Play("Attack");
            Attack();
        }
        if(Input.GetKeyDown(KeyCode.Mouse1))
        {
            timeAtButtonDown = timeCurrent;
            amThrowing = true;
        }

        if ((timeCurrent - timeAtButtonDown) >= 4 && amThrowing)
        {
            fuse = timeCurrent - timeAtButtonDown;
            //Use Item with no fuse
            UseItem1(fuse);
        }

        if (Input.GetKeyUp(KeyCode.Mouse1) && amThrowing)
        {
            timeAtButtonUp = timeCurrent;
            fuse = timeAtButtonUp - timeAtButtonDown;            //Use Item with Delta Time
            UseItem1(fuse);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            UseHealItem();
        }
    }

    void Attack()
    {
        //play animation
        animator.SetTrigger("playerAttack");

        // Detect enemies in range of Attack
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.transform.position, attackRange, enemyLayers);
        foreach(Collider2D enemy in hitEnemies)
        {
            enemy.GetComponentInChildren<Health>().GiveDamage(attackDamage);
        }
        // damage enemies


    }

    void UseItem1(float fuse)
    {
        if (Bombs >= 1)
        {
            player.GetComponent<Item>().Use("bomb", fuse);
            Bombs--;
            GameDirector.Instance.savedPlayerData.bombs = Bombs;
            amThrowing = false;
        } else
        {
            return;
        }
    }

    void UseHealItem()
    {
        double newHealth = GetComponentInChildren<Health>().GetHealth();

        if (healthPot >= 1 && newHealth < 5)
        {
            
            player.GetComponentInChildren<Health>().SetHealth(newHealth + 1);
            healthPot--;
            GameDirector.Instance.savedPlayerData.healthPot = healthPot;
        } 
        else
        {
            return;
        }

    }

    public double getBombAmount()
    {
        return Bombs;
    }

    public double getHealthPotAmount()
    {
        return healthPot;
    }
}
