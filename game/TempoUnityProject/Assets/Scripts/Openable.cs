using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]

public class Openable : Interactable
{
    [SerializeField] PlayerController player;
    public Sprite open;
    public Sprite closed;

    private SpriteRenderer sr;
    private bool isOpne;
    private int oreMined = 0;
    public override void Interact()
    {
        oreMined ++;
        player.MineInteraction();
        if(isOpne) sr.sprite = closed;
        Debug.Log(oreMined);
        if(oreMined >= 5){
            isOpne = !isOpne;
        }

        if(oreMined >= 20){
            Destroy(this.gameObject);
        }

    }

    private void Start() 
    {
        sr = GetComponent<SpriteRenderer>();
        sr.sprite = open;
    }
}
