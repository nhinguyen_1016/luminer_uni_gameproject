using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEnemyCollision : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        Health hit = other.GetComponentInChildren<Health>();

        if (other.gameObject.tag == "Enemy" && hit.GetHealth() >= 0)
        {
            Debug.Log("player collided with enemy");
            var magnitude = 5000;
            var force = transform.position - other.transform.position;
            force.Normalize();
            gameObject.GetComponent<Health>().GiveDamage(1);
            gameObject.GetComponentInParent<Rigidbody2D>().AddForce (force * magnitude);
            transform.parent.gameObject.transform.GetComponentInChildren<Animator>().SetTrigger("getsPushed");
        }
    }
}
