using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mining : MonoBehaviour
{
    float miningRadius;
    public int strength;
    private WorldTile tile;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<CircleCollider2D>().radius = miningRadius;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Mine(float miningRadius)
    {
        this.miningRadius = miningRadius;
        this.GetComponent<CircleCollider2D>().radius = miningRadius;
        if (tile.gameObject.CompareTag("Ore"))
        {
            tile.GetComponent<WorldTile>().Interact(true);

        } else {
            tile.GetComponent<WorldTile>().Interact(false);
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("World") || other.gameObject.CompareTag("Ore"))
        {
            tile = other.gameObject.GetComponent<WorldTile>();
        }
    }
}
