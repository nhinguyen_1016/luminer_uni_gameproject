using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopkeeperDialogue : MonoBehaviour
{
    [SerializeField] DialogueManager dM;
    public Dialogue dialogue;
    public Dialogue exitHint;

    private bool hasChatted = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player") && !hasChatted)
        {
            dM.StartDialogue(dialogue, true);           
            hasChatted = true;
        } else if(collision.CompareTag("Player"))
        {
            dM.StartDialogue(exitHint, true);
        }
    }
}
